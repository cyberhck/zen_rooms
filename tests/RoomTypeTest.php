<?php

/**
 * Created by PhpStorm.
 * User: Nishchal Gautam
 * Date: 5/18/16
 * Time: 9:49 PM
 */

class RoomTypeTest extends PHPUnit_Framework_TestCase {
	/**
	 * @return mysqli
	 */
	private function getDatabase(){
		$DB = $GLOBALS['DB'];
		return new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['DB']. '_test');
	}
	public function test_RoomType_exists() {
		self::assertEquals(TRUE, class_exists('RoomType'));
	}

	public function test_has_methods() {
		$db = $this->getDatabase();
		$room_type = new RoomType($db);
		self::assertTrue(method_exists($room_type, 'getDefaultPricing'), "Class doesn't have method getDefaultPricing");
		self::assertTrue(method_exists($room_type, 'getPricing'), "Class doesn't have method getPricing");
	}

	/**
	 * @param $room_type int
	 * @param $expected int
	 * @dataProvider getDefaultPricingShouldReturnCorrectValuesDataProvider
	 */
	public function test_getDefaultPricingShouldReturnCorrectValues($room_type, $expected){
		$db = $this->getDatabase();
		$roomType = new RoomType($db);
		self::assertEquals($expected, $roomType->getDefaultPricing($room_type));
	}
	public function getDefaultPricingShouldReturnCorrectValuesDataProvider(){
		return array(
			array(1,3000),
			array(2,5000)
		);
	}

	/**
	 * @param $room_type int
	 * @param $date      string
	 * @param $expected  int
	 *
	 * @dataProvider getPricingShouldReturnCorrectValuesDataProvider
	 * @throws \Exception
	 */
	public function test_getPricingShouldReturnCorrectValues($room_type, $date, $expected){
		$db = $this->getDatabase();
		$roomType = new RoomType($db);
		self::assertEquals($expected, $roomType->getPricing($room_type, $date)['price']);
		self::assertTrue(is_int($roomType->getPricing($room_type, $date)['rooms_available']));
	}
	public function getPricingShouldReturnCorrectValuesDataProvider(){
		return array(
			array(1,'2016-05-21',3600),
			array(1,'2016-05-22',3500),
			array(1,'2016-05-23',3400),
			array(1,'2016-05-24',3300),
			array(1,'2016-05-25',3200),
			array(1,'2016-05-26',3100),

			array(2,'2016-05-21',5600),
			array(2,'2016-05-22',5500),
			array(2,'2016-05-23',5400),
			array(2,'2016-05-24',5300),
			array(2,'2016-05-25',5200),
			array(2,'2016-05-26',5100)
		);
	}
}
