<?php

/**
 * Created by PhpStorm.
 * User: cyberkiller
 * Date: 5/18/16
 * Time: 6:40 PM
 * heavily uses that other node.js application, be careful with localhost:3000
 */
class RequestTest extends PHPUnit_Framework_TestCase {
	private $url = 'http://localhost:3000';
	public function test_request_exists() {
		self::assertEquals(TRUE, class_exists('Request'));
	}

	public function test_methods_exist() {
		$request = new Request;
		self::assertTrue(method_exists($request, 'setUrl'), 'Class has no method setUrl');
		self::assertTrue(method_exists($request, 'getUrl'), 'Class has no method getUrl');
		self::assertTrue(method_exists($request, 'GET'), 'Class has no method GET');
		self::assertTrue(method_exists($request, 'POST'), 'Class has no method POST');
		self::assertTrue(method_exists($request, 'PUT'), 'Class has no method PUT');
		self::assertTrue(method_exists($request, 'PATCH'), 'Class has no method PATCH');
		self::assertTrue(method_exists($request, 'setData'), 'Class has no method setData');
		self::assertTrue(method_exists($request, 'setDataType'), 'Class has no method setDataType');
	}

	/**
	 * @param $url
	 *
	 * @dataProvider setUrl_test_data_provider
	 */
	public function test_setUrl($url) {
		$request = new Request();
		$request->setUrl($url);
		self::assertEquals($url, $request->getUrl());
	}

	public function setUrl_test_data_provider() {
		return array(
			array('http://localhost:3000'),
			array('http://localhost:3000/'),
			array('//localhost:3000')
		);
	}
	public function test_GET(){
		$request = new Request();
		$request->setUrl($this->url);
		self::assertEquals('get', $request->GET(null));
	}
	public function test_POST(){
		//create a data
		$request = new Request();
		$request->setUrl($this->url);
		self::assertEquals('post', $request->POST());
	}
	public function test_PATCH(){
		//used for update, send only data which is to be updated
		$request = new Request();
		$request->setUrl($this->url);
		self::assertEquals('patch', $request->PATCH(null));
	}
	public function test_PUT(){
		//used for update (sends whole data)
		$request = new Request();
		$request->setUrl($this->url);
		self::assertEquals('put', $request->PUT(null));
	}

}
