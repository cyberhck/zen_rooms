var express = require('express');
var router = express.Router();
var cont = require('../controllers/index.js');
/* GET home page. */
router.get('/', function(req, res, next) {
	cont(req, res, next, 'get');
});
router.patch('/',function(req, res, next){
	cont(req, res, next, 'patch');
});
router.put('/',function(req, res, next){
	cont(req, res, next, 'put');
});
router.post('/',function(req, res, next){
	cont(req, res, next, 'post');
});
module.exports = router;
