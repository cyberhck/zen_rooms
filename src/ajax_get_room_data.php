<?php
require_once(__DIR__ . '/autoload.php');
$num_days_to_return = $_GET['days'];
$date_to_show_after = $_GET['date_start'];
$room_data = array();

if ($date_to_show_after === '') {
	$date_to_show_after = date('Y-m-d');
}
$DB = $GLOBALS['DB'];
$db = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['DB']);
$room_type = new RoomType($db);
for ($i = 0; $i < $num_days_to_return; $i++) {
	$date_after_i = date('Y-m-d', strtotime($date_to_show_after . "+{$i} days"));
	$room = array();
	$room['single'] = array(
		'date'  => $date_after_i,
		'room_info' => $room_type->getPricing(1, $date_after_i),
		'day'   => date('l', strtotime($date_to_show_after . "+{$i} days")),
	);
	$room['double'] = array(
		'date'  => $date_after_i,
		'room_info' => $room_type->getPricing(2, $date_after_i),
		'day'   => date('l', strtotime($date_to_show_after . "+{$i} days")),
	);
	$room_data[] = $room;
}
header('Content-Type: application/json');
$data['room_data'] = $room_data;
$data['next_date'] = date('Y-m-d', strtotime($date_to_show_after . "+{$i} days"));
echo json_encode($data);
