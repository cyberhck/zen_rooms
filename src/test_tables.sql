CREATE DATABASE IF NOT EXISTS zen_rooms_test;
USE zen_rooms_test;
CREATE TABLE IF NOT EXISTS room_type(
  id INT AUTO_INCREMENT PRIMARY KEY,
  room_type ENUM('single', 'double'),
  rooms_available INT DEFAULT 5,
  default_pricing INT
);
CREATE TABLE IF NOT EXISTS pricing(
  id INT PRIMARY KEY AUTO_INCREMENT,
  room_type INT,
  price INT,
  _date DATE,
  rooms_available INT DEFAULT 5,
  CONSTRAINT FOREIGN KEY (room_type) REFERENCES room_type(id)
);
INSERT INTO room_type (room_type, default_pricing, rooms_available) VALUES ('single',3000, 5);
INSERT INTO room_type (room_type, default_pricing, rooms_available) VALUES ('double',5000, 5);

INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (1, 3600, '2016-05-21', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (1, 3500, '2016-05-22', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (1, 3400, '2016-05-23', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (1, 3300, '2016-05-24', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (1, 3200, '2016-05-25', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (1, 3100, '2016-05-26', 5);

INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (2, 5600, '2016-05-21', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (2, 5500, '2016-05-22', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (2, 5400, '2016-05-23', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (2, 5300, '2016-05-24', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (2, 5200, '2016-05-25', 5);
INSERT INTO pricing (room_type, price, `_date`, rooms_available) VALUES (2, 5100, '2016-05-26', 5);