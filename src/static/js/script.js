var base_url = "http://localhost";

window.onload = function () {
    populateData();
};

function detect_scroll() {
    if ((window.innerHeight + window.scrollY) + 30 > document.body.offsetHeight) {
        populateData();
    }
}

window.onscroll = detect_scroll;

function update_room_data(data) {
    $.post(base_url + "/ajax_update.php", data, function (res) {
        window.location.reload();
    }, function (res) {
        console.log(res);
    });
}

function edit_price(element) {
    var date = getDateEdit(element);
    var room_type = element.getAttribute('data-type');
    var price = element.getAttribute('data-price');
    var price = prompt("Enter new price", price);
    //create a ajax request and send
    data = "date=" + date + "&price=" + price + "&room_type=" + room_type;
    update_room_data(data);
}
function edit_room(element) {
    var date = getDateEdit(element);
    var room_type = element.getAttribute('data-type');
    var rooms = element.getAttribute('data-rooms');
    var rooms = prompt("Enter number of rooms available", rooms);
    data = "date=" + date + "&rooms=" + rooms + "&room_type=" + room_type;
    update_room_data(data);
}
function getDateEdit(element) {
    return element.getAttribute('data-date');
}
function create_row(single, double) {
    var single_date = single.date;
    var single_price = single.room_info.price;
    var single_day = single.day;
    var double_price = double.room_info.price;
    var table_row = document.createElement('tr');
    if (single_day == "Saturday" || single_day == "Sunday") {
        table_row.classList.add('holiday');
    }
    var table_row_content =
        "<td class='day'>" + single_day + "</td>"
        + "<td>" + single_date + "</td>"

        + "<td onclick='edit_price(this)' data-type='single' data-price='" + single_price + "' data-date='" + single_date + "'>" + single_price + "</td>"
        + "<td onclick='edit_room(this)' data-type='single' data-rooms='" + single.room_info.rooms_available + "' data-date='" + single_date + "'>" + single.room_info.rooms_available + "</td>"

        + "<td onclick='edit_price(this)' data-type='double' data-price='" + single_price + "' data-date='" + single_date + "'>" + double_price + "</td>"
        + "<td onclick='edit_room(this)' data-type='double' data-rooms='" + double.room_info.rooms_available + "' data-date='" + single_date + "'>" + double.room_info.rooms_available + "</td>";

    table_row.innerHTML = table_row_content;
    return table_row;
}
function populateData() {
    window.onscroll = null;
    var num_days = document.getElementById("num_days_show").value;
    var date_start = document.getElementById('date_to_show_from').value;
    $.get(base_url + '/ajax_get_room_data.php?days=' + num_days + '&date_start=' + date_start, function (res) {
        var next_date = res.next_date;
        document.getElementById('date_to_show_from').value = next_date;
        var rooms_data = res.room_data;
        rooms_data.forEach(function (item, index) {
            var single_room = item.single;
            var double_room = item.double;
            //create a nice little row, by passing single room and double room
            var table_row = create_row(single_room, double_room);
            //insert at end of table_row
            document.querySelector("#rooms_editor tbody").appendChild(table_row);
        });
        //iterate through it and see
        window.onscroll = detect_scroll;
    }, function (res) {
        console.log(res);
    });
}
customObject = $ = {};
$.get = function (url, callback, error) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4 && ajax.status == 200) {
            callback(JSON.parse(ajax.responseText));
        } else if (ajax.readyState == 4) {
            error(ajax);
        }
    };
    ajax.open("GET", url, true);
    ajax.send();
};
$.post = function (url, data, callback, error) {
    var ajax = new XMLHttpRequest();
    ajax.onreadystatechange = function () {
        if (ajax.readyState == 4 && ajax.status == 200) {
            callback(JSON.parse(ajax.responseText));
        } else if (ajax.readyState == 4) {
            error(ajax);
        }
    };
    ajax.open("POST", url, true);
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // very bad example, should actually use json but since I can do a $_POST, for demo purpose,
    // I'm using this content type
    ajax.send(data);
};
