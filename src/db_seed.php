<?php
require_once __DIR__.'/autoload.php';
$command = "mysql -h {$DB['HOST']} -u {$DB['USER']} -p{$DB['PASS']}< src/tables.sql";
exec($command, $output, $error_code);
if(!$error_code){
	echo 'Database Seeded';
}
$command = "mysql -h {$DB['HOST']} -u {$DB['USER']} -p{$DB['PASS']}< src/test_tables.sql";
exec($command, $output, $error_code);
if(!$error_code){
	echo 'Test Database Seeded';
}
//send a sample seed to our 3rd party server

$DB = $GLOBALS['DB'];
$db = new mysqli($DB['HOST'], $DB['USER'], $DB['PASS'], $DB['DB']);
$roomType = new RoomType($db);
$roomType->syncDefault();
