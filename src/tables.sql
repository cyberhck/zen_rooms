CREATE DATABASE IF NOT EXISTS zen_rooms;
USE zen_rooms;
CREATE TABLE room_type(
  id INT AUTO_INCREMENT PRIMARY KEY,
  room_type ENUM('single', 'double'),
  rooms_available INT DEFAULT 5,
  default_pricing INT
);
CREATE TABLE IF NOT EXISTS pricing(
  id INT PRIMARY KEY AUTO_INCREMENT,
  room_type INT,
  price INT,
  _date DATE,
  rooms_available INT DEFAULT 5,
  CONSTRAINT FOREIGN KEY (room_type) REFERENCES room_type(id)
);
INSERT INTO room_type (room_type, default_pricing, rooms_available) VALUES ('single', 3000, 5);
INSERT INTO room_type (room_type, default_pricing, rooms_available) VALUES ('double', 5000, 5);
