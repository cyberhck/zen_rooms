<?php

/**
 * Created by PhpStorm.
 * User: cyberkiller
 * Date: 5/18/16
 * Time: 9:48 PM
 */
class RoomType {
	private $db;
	/**
	 * RoomType constructor.
	 *
	 * @param $db mysqli
	 */
	public function __construct($db) {
		$this->db = $db;
	}

	/**
	 * @param $room_type int room type
	 */
	public function getDefaultPricing($room_type){
		$sql = 'SELECT default_pricing as pricing, rooms_available,room_type FROM room_type WHERE room_type = ? LIMIT 1';
		$statement = $this->db->prepare($sql);
		$statement->bind_param('i', $room_type);
		if($statement->execute()){
			$result = $statement->get_result();
			if($result->num_rows){
				$data = $result->fetch_assoc();
				return $data['pricing'];
			}
		}
	}

	/**
	 * @param $room_type
	 *
	 * @return array defaults
	 */
	public function getDefaults($room_type){
		$sql = 'SELECT default_pricing as pricing, rooms_available,room_type FROM room_type WHERE room_type = ? LIMIT 1';
		$statement = $this->db->prepare($sql);
		$statement->bind_param('i', $room_type);
		if($statement->execute()){
			$result = $statement->get_result();
			if($result->num_rows){
				$data = $result->fetch_assoc();
				return $data;
			}
		}
	}

	/**
	 * @param $room_type int
	 * @param $date      string
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getPricing($room_type, $date){
		$sql = 'SELECT price,
				room_type,
				rooms_available
				FROM pricing
				WHERE `_date` = ?
				AND room_type = ?
					UNION ALL
				(SELECT default_pricing as price,
				room_type,
				rooms_available
				FROM room_type
				WHERE room_type = ?) LIMIT 1';
		$statement = $this->db->prepare($sql);
		$statement->bind_param('sii', $date, $room_type, $room_type);
		if($statement->execute()){
			$result = $statement->get_result();
			if($result->num_rows){
				$data = $result->fetch_assoc();
				return array(
					'price' => $data['price'],
					'rooms_available' => $data['rooms_available'],
					'room_type' =>$room_type,
					'date' => $date);
			}
			throw new Exception('Something went wrong');
		}
		throw new Exception('Error With Execution of query');
	}

	/**
	 * @param $date
	 * @param $room_type
	 * @param $rooms
	 * This function is not yet tested, please use with caution
	 *
	 * @throws \Exception
	 */
	public function updateRoomAvailable($date,$room_type,$rooms){
		if($room_type == 'single'){
			$room_type = 1;
		}else if($room_type == 'double'){
			$room_type = 2;
		}else{
			header('HTTP/1.1 400 Bad Request');
			header('Content-Type: application/json');
			die("{error:'true'}");
		}
		$sql = 'SELECT * FROM pricing WHERE `_date` = ? AND room_type = ?';
		$statement = $this->db->prepare($sql);
		$statement->bind_param('si', $date, $room_type);
		$statement->execute();
		if($statement->execute()){
			$result = $statement->get_result();
			if($result->num_rows){
				$sql = 'UPDATE pricing SET rooms_available = ? WHERE `_date` = ? AND room_type = ?';
				$statement = $this->db->prepare($sql);
				$statement->bind_param('isi', $rooms, $date, $room_type);
				$statement->execute();
				header('Content-Type: application/json');
				$data['status'] = 'ok';
				//sync to 3rd party
				$this->sync($date);
				die(json_encode($date));
			}else{
				$price = $this->getDefaultPricing($room_type);
				$sql = 'INSERT INTO pricing 
						(room_type, price, `_date`, rooms_available)
						 VALUES (?, ?, ?, ?)';
				$statement = $this->db->prepare($sql);
				$statement->bind_param('iisi', $room_type, $price, $date, $rooms);
				$statement->execute();
				header('Content-Type: application/json');
				$data['status'] = 'ok';
				$this->sync($date);
				//sync to 3rd party
				die(json_encode($date));
			}
			throw new Exception('Something went wrong');
		}
		throw new Exception('Error With Execution of query');
	}
	private function sync($date){
		$sync_1 = $this->getPricing(1, $date);
		$sync_2 = $this->getPricing(2, $date);
		$request = new Request('http://localhost:3000');
		$request->POST($sync_1);
		$request->POST($sync_2);
	}
	public function syncDefault(){
		$sync1=$this->getDefaults(1);
		$sync2=$this->getDefaults(2);
		$request = new Request('http://localhost:3000');
		$request->POST($sync1);
		$request->POST($sync2);
	}
	public function updatePrice($date, $room_type, $price){
		if($room_type == 'single'){
			$room_type = 1;
		}else if($room_type == 'double'){
			$room_type = 2;
		}else{
			header('HTTP/1.1 400 Bad Request');
			header('Content-Type: application/json');
			die("{error:'true'}");
		}

		$sql = 'SELECT * FROM pricing WHERE `_date` = ? AND room_type = ?';
		$statement = $this->db->prepare($sql);
		$statement->bind_param('si', $date, $room_type);
		$statement->execute();
		if($statement->execute()){
			$result = $statement->get_result();
			if($result->num_rows){
				$sql = 'UPDATE pricing SET price = ? WHERE `_date` = ? AND room_type = ?';
				$statement = $this->db->prepare($sql);
				$statement->bind_param('isi', $price, $date, $room_type);
				$statement->execute();
				header('Content-Type: application/json');
				$data['status'] = 'ok';
				$this->sync($date);
				die(json_encode($date));
			}else{
				$sql = 'INSERT INTO pricing 
						(room_type, price, `_date`, rooms_available)
						 VALUES (?, ?, ?, 5)';
				$statement = $this->db->prepare($sql);
				$statement->bind_param('iis', $room_type, $price, $date);
				$statement->execute();
				header('Content-Type: application/json');
				$data['status'] = 'ok';
				$this->sync($date);
				die(json_encode($date));
			}
			throw new Exception('Something went wrong');
		}
		throw new Exception('Error With Execution of query');

	}
}
