<?php

/**
 * Class Request
 * Class to make requests, currently only supports GET, POST, PATCH, PUT
 */
class Request {
	private $url;
	private $data;

	private $data_type = 'application/json';

	/**
	 * Request constructor.
	 *
	 * @param $url string URL to make request to
	 */
	public function __construct($url = NULL) {
		//todo: should check URL if it's valid
		$this->url = $url;
	}


	/**
	 * @return string
	 */
	public function getUrl() {
		return $this->url;
	}

	/**
	 * @param $url string
	 */
	public function setUrl($url) {
		$this->url = $url;
	}

	/**
	 * @param $data mixed $data to send
	 */
	public function setData($data) {
		$this->data = $data;
	}

	/**
	 * @return mixed
	 */
	public function getData() {
		return $this->data;
	}

	/**
	 * @param $ch
	 *
	 * @return string|false
	 */
	private function execute($ch){
		$result = curl_exec($ch);
		return $result;
	}

	/**
	 * @param $data mixed data to send
	 *
	 * @return false|string
	 */
	public function GET($data) {
		$ch = $this->getCurlInstance();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		return $this->execute($ch);
	}

	/**
	 * @param $data mixed data to send
	 *
	 * @return false|string
	 */
	public function POST($data=[]) {
		$ch = $this->getCurlInstance();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		return $this->execute($ch);
	}

	/**
	 * @param $data mixed data to send
	 *
	 * @return false|string
	 */
	public function PATCH($data) {
		$ch = $this->getCurlInstance();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PATCH');
		return $this->execute($ch);
	}

	/**
	 * @param $data mixed data to send
	 *
	 * @return false|string
	 */
	public function PUT($data=null) {
		$ch = $this->getCurlInstance();
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		return $this->execute($ch);
	}

	/**
	 * @return string
	 */
	public function getDataType() {
		return $this->data_type;
	}

	/**
	 * @param string $data_type
	 */
	public function setDataType($data_type) {
		$this->data_type = $data_type;
	}

	/**
	 * @return resource
	 */
	private function getCurlInstance(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$this->url);
		curl_setopt($ch, CURLINFO_CONTENT_TYPE, $this->data_type);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return $ch;
	}

}
