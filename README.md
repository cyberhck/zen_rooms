# Zen Rooms Demo

Directory `src` should be root for webserver.

First do a `cp src/config.php src/config.local.php` and update the 
values in config.local.php for user and password And please use
zen_rooms for name of database since I'm hardcoding that on sql file

Please note that I didn't want to include a lot of libraries so I'm
seeding a test database for testing purposes,

# Installing dependencies:

Since this application has two application (one is for testing)
we have to do `npm install` on `src` directory and `npm install` on
`sample_request` directory.
 
To seed database, you can just use `npm run seed` since that's when
we initialize default pricing to 3rd party API server, we can actually
run `npm run start` in one terminal and when it starts, run 
`npm run seed` in other terminal and if we look at the first terminal
while seeding, we will see two post request on the log with the data

After seeding is done, we need to kill that node.js. (Please use
`Ctrl + C` and not `Ctrl + Z`)

Finally open `src/static/js/script.js` with your favorite text editor 
and modify base_url to your config (most likely `localhost`)

# Running tests

Just using phpunit won't work as it also has tests for requests, 
for that there's a sample node app, so to test, do a npm run test which 
start node server, wait 3 seconds to initialize and then run all the 
test cases

#  Execution
when all the tests are passing, go ahead and view the stuff on browser,
But make sure that you have run node.js app first, as it is the mock
api which is in sync

To start node.js app, you can run `npm run start`. After we run node app
we can use our application and whenever we make an update on browser, it
actually makes an additional request to 3rd party server (this case
`localhost:3000`) which will be shown as a log in our terminal

# Documentation
I'm using apigen.phar to generate documentation. You can run 
`npm run documentation` and go to localhost:{PORT} (PORT depends where
python exposes, mostly it's 8000)

## Windows Users,
Sorry, since I don't use that all, I really don't know how to do npm
stuffs on windows, it's been quiet a long time since I did development
on that platform. I'm pretty sure someone can help figure out.
